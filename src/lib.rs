#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/openzen-bindings.rs"));

use std::ffi::CString;

//use openzen_rust::ZenSensorComponentGnnsForwardRtkCorrections;

pub fn connect_and_stream() {
    //println!("called rary's `public_function()`");
    let mut hd : ZenClientHandle_t = ZenClientHandle{ handle : 0 };
    let hd_ptr: *mut ZenClientHandle_t = &mut hd;

    let mut hd_sensor : ZenSensorHandle_t = ZenSensorHandle{ handle : 0 };
    let hd_sensor_ptr: *mut ZenSensorHandle_t = &mut hd_sensor;

    let mut event: ZenEvent = ZenEvent::default();
    let event_ptr: *mut ZenEvent = &mut event;
    
    let io_name = CString::new("LinuxDevice").expect("CString::new failed");
    let dev_name = CString::new("LPMSCU2000003").expect("CString::new failed");

    unsafe {
        let err = ZenInit(hd_ptr);
        if err != ZenSensorInitError_ZenSensorInitError_None {
            println!("No error");
            return;
        }

        println!("OpenZen created");

        let err_sensor = ZenObtainSensorByName(hd, io_name.as_ptr(), dev_name.as_ptr(), 0, hd_sensor_ptr);
        if err_sensor != ZenSensorInitError_ZenSensorInitError_None {
            println!("Cannot connect sensor");
            return;
        }

        for _n in 1..101 {
            ZenWaitForNextEvent(hd, event_ptr);
            println!("A: {} {} {}",
                event.data.imuData.a[0], event.data.imuData.a[1], event.data.imuData.a[2]);
        }

        ZenShutdown(hd);
    }
}


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {        
    }
}
