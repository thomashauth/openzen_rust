# OpenZen bindings for Rust

To compile and run:
```
git clone git@bitbucket.org:thomashauth/openzen_rust.git
cd openzen_rust/
git submodule update --init --recursive
cargo build
cargo run
```
